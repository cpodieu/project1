from django.shortcuts import render, redirect, HttpResponse
from .models import Quote
from demo_app.models import User
from django.contrib import messages



def quote(request):

    context = {
        "quotes" : Quote.objects.filter(creator = User.objects.get(id = request.session['user_id']))
    }
    # "quotes" : Quote.objects.filter(creator = User.objects.get(id = request.session['user_id']))
    return render(request, "quotes.html", context)

def quotes(request):
    if request.method == 'POST':
        errors = Quote.objects.basic_validator(request.POST)
        if len(errors):
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/quotes')
        else:
            Quote.objects.create(title=request.POST['title'], description=request.POST['description'], creator=User.objects.get(
                id=request.session['user_id']))
            print(request.POST)    
            return redirect('/quotes')
    else:
        return redirect('/quotes')

def edit(request, id):
    context = {
        'user': User.objects.get(email=request.session['email']),
        'quote': Quote.objects.get(id=id)
    }
    return render(request, "edit.html", context)       

def update(request, id):
    if request.method == 'POST':
        errors = Quote.objects.basic_validator(request.POST)
        if len(errors):
            for k, v in errors.items():
                messages.error(request, v)
            return redirect('/edit/'+id)
        else:          
            quote = Quote.objects.get(id = request.POST['quote_id'])
            quote.title = request.POST['title']
            quote.description = request.POST['description']
            quote.save()
            return redirect('/quotes')
    else:
        return redirect('/')  

def delete(request):
    if request.method == 'POST':
        quote = Quote.objects.get(id=request.POST['quote_id'])
        quote.delete()
        return redirect('/quotes')
    else:
        return redirect('/') 

def count(request, id):
    context = {
        'user': User.objects.get(email=request.session['email']),
        'quote': Quote.objects.count(),
        "quotes" : Quote.objects.filter(creator = User.objects.get(id = request.session['user_id']))
    }
    return render(request, 'count.html', context)
