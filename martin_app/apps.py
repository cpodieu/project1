from django.apps import AppConfig


class MartinAppConfig(AppConfig):
    name = 'martin_app'
