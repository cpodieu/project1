from django.db import models
from demo_app.models import User

class QuoteManager(models.Manager):

    def basic_validator(self, post_data):
        errors ={}

        if len(post_data['title']) < 2:
            errors['title'] = "quote name is less two three charateres."
        if len(post_data['description']) < 10:
            errors['decription'] = "should be at least ten characters"
        return errors    



class Quote(models.Model):
    title = models.CharField(max_length = 255)
    description = models.TextField()
    creator = models.ForeignKey(User, related_name = "quotes", on_delete = models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = QuoteManager()
