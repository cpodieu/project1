from django.urls import path
from . import views

urlpatterns = [
    path('quotes', views.quote),
    path('display', views.quotes),
    path('edit/<int:id>', views.edit),
    path('update/<int:id>', views.update),
    path('delete', views.delete),
    path('users/<int:id>', views.count)
]