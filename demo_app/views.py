from django.shortcuts import render, redirect, HttpResponse
from django.contrib import messages
from .models import User
import bcrypt

def index(request):
    return render(request, "index.html")

def register(request):
    errors = User.objects.basic_validator(request.POST)
    if errors:
        for k, v in errors.items():
            messages.error(request, v)
        return redirect('/') 
    print('Nice!!!!!!')
    pw_hash = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt()).decode()

    User.objects.create(
        first_name = request.POST['first_name'],
        last_name = request.POST['last_name'],
        email = request.POST['email'],
        password = pw_hash
    )
    messages.info(request, "User create, please log in.")
    return redirect('/')

def login(request):
    try:
        user = User.objects.get(email = request.POST['email'])
    except:
        messages.error(request, 'Wrong email or password.')
        return redirect('/')

    if bcrypt.checkpw(request.POST['password'].encode(), user.password.encode()):
        print('password matche!!!!')
    else:
        messages.error(request, 'Be informed that you are still using incorrect email or password.')
        return redirect('/') 
    request.session['user_id'] = user.id
    request.session['first_name'] = user.first_name
    request.session['last_name'] = user.last_name
    request.session['email'] = user.email
     
    return redirect('/quotes')

# def success(request):
#     if not 'user_id' in request.session:
#         messages.error(request, "log in please.")
#         return redirect('/')
#     return render(request, "success.html")        

def logout(request):
    try:
        del request.session['user_id']
        del request.session['first_name']    
        del request.session['last_name']
    except:
        pass    
    messages.info(request, "Thank you!!! see you Next Time")
    return redirect('/')