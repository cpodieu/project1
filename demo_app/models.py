from django.db import models
import re
class UserManager(models.Manager):

    def basic_validator(self, post_data):
        errors = {}

        email_regex = re.compile(r'^[a-zA-Z0-9.+-_]+@[a-zA-Z0-9.-_]+\.[a-zA-Z]+$')
        if not email_regex.match(post_data['email']):
            errors['email'] = 'Invalid email address.'

        if (len(post_data['first_name']) < 2):
            errors['first_name'] = 'First name should be 2 or more characters long.'

        if (len(post_data['last_name']) < 2):
            errors['last_name'] = 'Last name should be 2 or more characters long.'    

        if len(post_data['password']) < 10:
            errors['password'] = 'password should be at least ten characters long.'

        if post_data['password'] != post_data['confirm_password']:
            errors['password_match'] = "password and confirmed password do not match"

        return errors    


def login_validator(self, post_data):
        errors = {}
        try:
            User.objects.get(email = post_data['email'])
            errors['email'] = 'This email is already in use.'
        except:
            pass 
        return errors        



class User(models.Model):
    first_name = models.CharField(max_length = 35)
    last_name = models.CharField(max_length = 35)
    email = models.EmailField(max_length=40)
    password = models.CharField(max_length = 60)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()